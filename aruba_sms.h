/**
 *  \file
 *  \remark This file is part of ArubaSms.
 *
 *  \copyright Copyright (C) 2018 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#if !defined(ARUBA_SMS)
#define      ARUBA_SMS

#include <memory>
#include <set>

#include <Data.Bind.Components.hpp>
#include <Data.Bind.ObjectScope.hpp>
#include <IPPeerClient.hpp>
#include <REST.Client.hpp>

namespace aruba::sms
{

extern const String type_alta;
extern const String type_standard;

struct PhoneNumber
{
  enum class exception {invalid_number};

  explicit PhoneNumber(const String & = "");

  String number;
};  // struct PhoneNumber

bool operator<(const PhoneNumber &, const PhoneNumber &);


class SMS
{
public:
  enum class exception {invalid_message_type, invalid_sender};

  SMS();

  [[nodiscard]] String message() const { return message_; }
  bool message(const String &);

  [[nodiscard]]  const String &order_id() const { return order_id_; }
  void order_id(const String &);

  [[nodiscard]] const String &sender() const { return sender_; }
  void sender(const String &);

  [[nodiscard]] const String &type() const { return type_; }
  void type(const String &);

  [[nodiscard]] const std::set<PhoneNumber> &recipients() const { return recipients_; }
  bool add_recipient(const String &);
  bool delete_recipient(const String &);

  [[nodiscard]] int length() const;
  [[nodiscard]] int count_sms() const;

private:
  String order_id_;                   /// SMS order ID.
  String type_;                       /// SMS type.
  String message_;                    /// SMS text.
  String sender_;                     /// SMS sender.
  std::set<PhoneNumber> recipients_;  /// The set of recipients.
};  // class SMS


class Connection
{
public:
  enum class exception {invalid_credentials, invalid_response,
                        invalid_message_length, missing_recipient,
                        session_not_available};

  Connection(const String &, const String &);

  [[nodiscard]] int credits(const String & = type_standard) const;
  [[nodiscard]] bool verify_session() const;

  struct SendResult
  {
    String order_id;
    int    sent_sms;
  };
  SendResult send(const SMS &) const;

private:
  [[nodiscard]] std::pair<std::unique_ptr<TRESTRequest>,
                          std::unique_ptr<TRESTResponse>> request_response_pair() const;

  std::unique_ptr<TRESTClient> client_;

  String user_key_;
  String session_key_;
};  // class Connection

}  // namespace aruba::sms

#endif  // include guard
