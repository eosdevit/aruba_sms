/**
 *  \file
 *  \remark This file is part of ArubaSms.
 *
 *  \copyright Copyright (C) 2018 EOS di Manlio Morini.
 *
 *  \license
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this file,
 *  You can obtain one at http://mozilla.org/MPL/2.0/
 */

#include <cassert>

#include <System.JSON.hpp>

#include <aruba_sms.h>

namespace
{

const String BASEURL("https://smspanel.aruba.it/API/v1.0/REST/");

void add_header_param(TRESTRequestParameterList *p,
                      const String &key, const String &val)
{
  p->AddItem(key, val);
  p->ParameterByName(key)->ContentTypeStr = "application/json";
  p->ParameterByName(key)->Kind = pkHTTPHEADER;
}

String strip_non_numeric(const String &in)
{
  String out;

  for (const auto &c : in)
    if (std::isdigit(c) || c == '+')
      out += c;

  return out;
}

// The message origin, also known as TPOA (Transmission Path Originating
// Address, is the SMS header field that contains the message sender's number.
//
// The TPOA field is limited by GSM standards to:
// - maximum 16 digits if the origin is numeric (e.g. a phone number);
// - maximum 11 alphabet characters and digits if the origin is alphanumeric
//   (e.g. a company name).
bool is_valid_tpoa(const String &s)
{
  if (s.Length() < 8)
    return false;

  if (s[1] == '0' && s[2] == '0')  // `00[0-9]{7,16}` format
  {
    int count(0);
    for (int i(3); i <= s.Length(); ++i)
      if (!std::isdigit(s[i]))
        return false;
      else
        ++count;

    return 7 <= count && count <= 16;
  }
  else if (s[1] == '+')            // `\\+[0-9]{7,16}` format
  {
    int count(0);
    for (int i(2); i <= s.Length(); ++i)
      if (!std::isdigit(s[i]))
        return false;
      else
        ++count;

    return 7 <= count && count <= 16;
  }

  return s.Length() < 12;
}

}  // unnamed namespace


namespace aruba::sms
{

const String type_alta("N");
const String type_standard("GP");

/// *********************** PhoneNumber ***********************
PhoneNumber::PhoneNumber(const String &n) : number(strip_non_numeric(n))
{
  if (number.IsEmpty())
    throw exception::invalid_number;

  if (number.Pos("00") == 1)
  {
    if (number.Length() == 2)
      throw exception::invalid_number;
  }
  else if (n.Pos("+") == 1)
  {}
  else
    number = "+39" + number;
}

bool operator<(const PhoneNumber &lhs, const PhoneNumber &rhs)
{
  return lhs.number < rhs.number;
}


/// *********************** SMS ***********************
SMS::SMS() : order_id_(), type_(type_standard), message_(), sender_(), recipients_()
{
}

/// Sets the message body.
///
/// \return `false` if the messagge is too short / long
bool SMS::message(const String &m)
{
  message_ = m;

  return !message_.IsEmpty() && length() <= 1000;
}

/// Sets the message ID.
///
/// \param[in] id order id (assigned by the server when empty)
///
/// \warning Maximum ID length is 32 characters.
void SMS::order_id(const String &id)
{
  order_id_ = id;
  order_id_ = order_id_.SubString(1, 32);
}

void SMS::sender(const String &s)
{
  if (!is_valid_tpoa(s))
    throw exception::invalid_sender;

  sender_ = s;
}

void SMS::type(const String &t)
{
  if (t != type_alta && t != type_standard)
    throw exception::invalid_message_type;

  type_ = t;
}

bool SMS::add_recipient(const String &r)
{
  try
  {
    recipients_.emplace(r);
    return true;
  }
  catch (PhoneNumber::exception)
  {
    return false;
  }
}

bool SMS::delete_recipient(const String &r)
{
  try
  {
    PhoneNumber pn(r);
    const auto elem(recipients_.find(pn));
    if (elem == recipients_.end())
      return false;

    recipients_.erase(elem);
    return true;
  }
  catch (PhoneNumber::exception)
  {
    return false;
  }
}

int SMS::count_sms() const
{
  const int length(message_.Length());
  return length <= 160 ? 1 : ((length - 1) / 153) + 1;
}

int SMS::length() const
{
  int len(0);

  for (auto c : message_)
    switch (c)
    {
      case '|':
      case '^':
      case '}':
      case '{':
      case '[':
      case '~':
      case ']':
      case '\\':  ++len;  // fall-through
      default:    ++len;
    }

  return len;
}


/// *********************** Connection ***********************
Connection::Connection(const String &user, const String &pwd)
  : client_(nullptr),
    user_key_(), session_key_()
{
  client_ = std::make_unique<TRESTClient>(nullptr);

  client_->BaseURL = BASEURL;

  client_->Params->AddItem("username", user);
  client_->Params->AddItem("password", pwd);

  auto request(std::make_unique<TRESTRequest>(nullptr));
  request->Client = client_.get();
  request->Resource = "login";

  auto response(std::make_unique<TRESTResponse>(nullptr));
  request->Response = response.get();
  request->Execute();

  if (response->StatusCode != 200)
    throw exception::invalid_credentials;

  auto ra(SplitString(response->Content, ";"));
  if (ra.size() != 2)
    throw exception::invalid_response;

  user_key_ = ra[0];
  session_key_ = ra[1];

  if (user_key_.IsEmpty() || session_key_.IsEmpty())
    throw exception::invalid_response;
}

std::pair<std::unique_ptr<TRESTRequest>, std::unique_ptr<TRESTResponse>>
Connection::request_response_pair() const
{
  if (user_key_.IsEmpty() || session_key_.IsEmpty())
    throw exception::session_not_available;

  auto request(std::make_unique<TRESTRequest>(nullptr));
  request->Client = client_.get();

  add_header_param(request->Params, "user_key", user_key_);
  add_header_param(request->Params, "Session_key", session_key_);

  auto response(std::make_unique<TRESTResponse>(nullptr));
  request->Response = response.get();

  return {std::move(request), std::move(response)};
}

/// Checks whether the user session is still active and valid (without renewal).
///
/// \return `true` if the session is valid, `false` otherwise
bool Connection::verify_session() const
{
  auto [request, response] = request_response_pair();

  request->Resource = "checksession";
  request->Execute();

  if (response->StatusCode == 401)
    throw exception::invalid_credentials;

  return response->StatusCode == 200;
}

/// Retrieves information about the credits.
///
/// \return number of sms available
int Connection::credits(const String &type) const
{
  auto [request, response] = request_response_pair();

  response->RootElement = "sms";
  request->Resource = "status";
  request->Execute();

  if (response->StatusCode == 401 || response->StatusCode == 404)
    throw exception::invalid_credentials;
  if (response->StatusCode != 200)
    throw exception::invalid_response;
  if (!response->JSONValue)
    throw exception::invalid_response;

  if (auto *jsa = dynamic_cast<TJSONArray *>(response->JSONValue))
    for (int i(0); i < jsa->Count; ++i)
      if (auto *jso = dynamic_cast<TJSONObject *>(jsa->Items[i]))
        if (jso->GetValue("type") && jso->GetValue("type")->Value() == type
            && jso->GetValue("quantity"))
          return StrToIntDef(jso->GetValue("quantity")->Value(), -1);

  throw exception::invalid_response;
}

/// Uses the active connection to send a SMS.
///
/// \param[in] sms message to send
/// \return        `true` if SMSs have been scheduled for delivery
Connection::SendResult Connection::send(const SMS &sms) const
{
  if (sms.recipients().empty())
    throw exception::missing_recipient;

  if (sms.length() <= 0 || sms.length() > 1000)
    throw exception::invalid_message_length;

  auto [request, response] = request_response_pair();

  request->Resource = "sms";

  auto payload(std::make_unique<TJSONObject>());

  payload->AddPair("message_type", sms.type());
  payload->AddPair("message", sms.message());
  if (!sms.sender().IsEmpty())
    payload->AddPair("sender", sms.sender());
  if (!sms.order_id().IsEmpty())
    payload->AddPair("order_id", sms.order_id());

  auto *recipients(new TJSONArray());
  payload->AddPair("recipient", recipients);

  for (const auto &r : sms.recipients())
    recipients->Add(r.number);

  request->Method = rmPOST;
  request->AddBody(payload->ToJSON(), ctAPPLICATION_JSON);
  request->Execute();

  if (response->StatusCode == 401 || response->StatusCode == 404)
    throw exception::invalid_credentials;
  if (response->StatusCode != 201)
    throw exception::invalid_response;

  if (auto *jso = dynamic_cast<TJSONObject *>(response->JSONValue))
    if (auto *oid = jso->GetValue("order_id"))
      if (auto *ts = jso->GetValue("total_sent"))
      {
        const String order_id(oid->Value());
        const String total_sent(ts->Value());

        return {order_id, StrToIntDef(total_sent, 0)};
      }

  throw exception::invalid_response;
}

}  // namespace arub::sms
