# [ARUBA SMS][1] #

## Panoramica ##

Libreria C++ per integrare in modo semplice il servizio SMS Aruba all'interno dei propri programmi.

L'integrazione, grazie alle API e agli SDK, è l'ideale se si desidera raggiungere direttamente il cellulare degli utenti in modo semplice.

Alcuni possibili utilizzi dell'integrazione sono:

- invio e ricezione SMS direttamente dai propri applicativi;
- impostazione di invio SMS su sistemi di allerta e segnalazioni guasti;
- servizi M2M;
- automazione messaggi SMS service.

Il codice è rilasciato con licenza [Mozilla Public License v2.0][3] così da permettere un'ampia flessibilità d'uso ed una semplice integrazione.

## Compilazione ##

La libreria è pensata per l'uso in ambiente [C++Builder][4], in particolare con versioni abbastanza recenti da incorporare il modulo [`REST Client`][2] (`TRESTClient`, `TRESTRequest`, `TRESTResponse`).

Lo sviluppo avviene con C++Builder Alexandria, per questa ragione l'uso di versioni precedenti potrebbe richiedere vari aggiustamenti.

In caso di problemi si prega di utilizzare il [tracking system](https://bitbucket.org/eosdevit/aruba_sms/issues); eventuali *patch* per risolverli sono più che gradite!

## Limitazioni ##

I server Aruba mettono a disposizione dello sviluppatore alcune interfacce di comunicazione HTTP per l'invio e la ricezione dei messaggi, la verifica di validità dei numeri di telefono ed il recupero di tutti i dati riguardanti lo stato dei messaggi e lo storico.

Questa libreria, al momento, supporta solo l'invio (non posticipato) dei messaggi e la verifica del credito disponibile.

## Guida di riferimento ##

La struttura della libreria è modellata seguendo, a grandi linee, il codice sorgente di [Aruba][5].

### Creazione di una connessione ###

Tutte le operazioni avvengono attraverso l'interfaccia `aruba::sms::Connection`. È possibile creare un oggetto `Connection` tramite il costruttore:

```c++
Connection(const String &username, const String &password);
```

Specificate le proprie credenziali di autenticazione, il servizio di Aruba comunica una *session key* temporanea.

Tutte le successive chiamate API utilizzeranno questa *session key*.

### Invio di SMS ###

La classe `aruba::sms::SMS` contiene tutti i dati necessari per inviare un messaggio SMS attraverso il *gateway* Aruba.

Creato un oggetto di tipo `SMS`, è necessario impostare:

- almeno **un destinatario**. Il numero di telefono dei destinatari del messaggio in formato internazionale; per aggiungere un numero alla lista dei destinatari, utilizzare il metodo `add_recipient`;
- il **corpo del messaggio**. Il testo viene specificato tramite il metodo `message`: massimo 160 caratteri per un'unico SMS, massimo 1000 caratteri per un SMS multiplo (concatenato).
  **ATTENZIONE**: i caratteri `^` `{` `}` `\` `[` `~` `]` `|` `€` vengono calcolati come 2 caratteri; è possibile utilizzare il metodo `count_sms` per conoscere il numero di SMS necessari per effettuare l'invio.

Opzionalmente, si possono definire:

- una stringa alfanumerica da associare all'invio. Indispensabile per ottenere successivamente le informazioni riguardanti lo stato dei messaggi (metodo `order_id`). La lunghezza massima è di 32 caratteri, tutti i caratteri eccedenti non vengono considerati.
  Se l'`order_id`non viene specificato, il server ne genererà uno garantito univoco.
  L'`order_id` potrà essere utilizzato per richiedere lo stato del messaggio per i 30 giorni successivi all'invio.

---

```c++
aruba::sms::Connection c(nome_utente, password);
aruba::sms::SMS sms;

sms.add_recipient(numero_destinatario);
sms.message(messaggio);

const auto ret(c.send(sms_));

if (ret.sent_sms < 1)
{
  // errore, invio fallito
}
```

### Credito SMS disponibile ###

Il metodo `credits` dell'oggetto `Connection` richiede al server Aruba l'effettiva disponibilità di credito (restituendo un `int`).

---

```c++
// Legge i crediti ad "alta disponibilità" residui
aruba::sms::Connection c(nome_utente, password);

std::cout << c.credits(aruba::sms::type_alta) << '\n';
```

Se non viene specificato un parametro si assume, come tipologia di credito, `aruba::sms::type_alta`.


[1]: https://bitbucket.org/eosdevit/aruba_sms/
[2]: https://docwiki.embarcadero.com/RADStudio/Alexandria/en/REST_Client_Library
[3]: https://bitbucket.org/eosdevit/aruba_sms/src/default/LICENSE
[4]: https://www.embarcadero.com/products/cbuilder
[5]: https://smsdevelopers.aruba.it/

